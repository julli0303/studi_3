import math
a = int(input("Введите значение а: "))
b = int(input("Введите значение b: "))
c = int(input("Введите значение c: "))
D = (b ** 2) - (4 * a * c)
if D > 0:
    x1 = (-b + math.sqrt(D)) / (2 * a)
    x2 = (-b - math.sqrt(D)) / (2 * a)
    print(str(x1), str(x2))
elif D == 0:
    x = (-b + math.sqrt(D)) / (2 * a)
    print(str(x))
else:
    print("Нет действительных корней")

